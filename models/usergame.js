'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Usergame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Usergame.hasOne(models.User_game_biodata, { foreignKey: 'user_game_id', as: 'biodata' })
      Usergame.hasMany(models.User_game_history, { foreignKey: 'user_game_id', as: 'userGame' })
    }
  }
  Usergame.init({
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    video: DataTypes.STRING,
    email: DataTypes.STRING,
    forgot_pass_token: DataTypes.STRING,
    forgot_pass_token_expired_at: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Usergame',
  });
  return Usergame;
};
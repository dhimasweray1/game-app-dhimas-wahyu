'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_game_biodata.belongsTo(models.Usergame, { foreignKey: 'user_game_id', as:'biodata'})
    }
  }
  User_game_biodata.init({
    fullName: DataTypes.STRING,
    date_of_birth: DataTypes.DATE,
    email: DataTypes.STRING,
    user_game_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User_game_biodata',
  });
  return User_game_biodata;
};
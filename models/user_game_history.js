'use strict';
const {
  Model
} = require('sequelize');
const user_game_biodata = require('./user_game_biodata');
module.exports = (sequelize, DataTypes) => {
  class User_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_game_history.belongsTo(models.Usergame, {through: 'user_game_history', foreignKey: 'user_game_id'})
    }
  }
  User_game_history.init({
    total_score: DataTypes.INTEGER,
    last_match: DataTypes.DATE,
    user_game_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User_game_history',
  });
  return User_game_history;
};
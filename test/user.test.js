const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const jwt = require('jsonwebtoken');
const app = require('../app')
// sebelum test, kita butuh data user
let token;


beforeEach(async () => {
  await queryInterface.bulkInsert('Usergames', [
    {
      name: "dhimas",
      password: "di",
      createdAt: new Date(),
      updatedAt: new Date()
    },
  ])
  token = jwt.sign({
    id: 1,
    email: 'dhimas'
  }, 'di')
})

afterEach(async () => {
  await queryInterface.bulkDelete('Usergames', {}, { truncate: true, restartIdentity: true })
})


describe('POST User', () => {
  it('success', (done) => {
    request(app)
    .post('/user')
    .set("authorization", token)
    .send({
      name: "sofia",
      password: "sofia123",
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('succesfully create user')
        done()
      }
    })
  })
  it('empty name', (done) => {
    request(app)
    .post('/user')
    .send({
      name: "",
      password: "",
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(400)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Data cannot be empty')
        done()
      }
    })
  })
  it('empty password', (done) => {
    request(app)
    .post('/user')
    .send({
      name: "",
      password: "",
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(400)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Data cannot be empty')
        done()
      }
    })
  })
})

describe('GET user', () => {
  it('success', (done) => {
    request(app)
      .get('/user')
      .set('authorization', token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(Array.isArray(res.body)).toBe(true)
        done()
      }
    })
  })
   it('No auth', (done) => {
    request(app)
    .get('/user')
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})


describe('GET todo by ID', () => {
  it('Success', (done) => {
    request(app)
    .get('/user/1')
    .set('authorization', token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('id')
        expect(res.body).toHaveProperty('name')
        expect(res.body).toHaveProperty('password')
        done()
      }
    })
  })
  it('Not found', (done) => {
    request(app)
    .get('/user/1000')
    .set('authorization', token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(404)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('user not found')
        done()
      }
    })
  })
  
})

describe('UPDATE /user/:id', () => {
  it('Success', (done) => {
    request(app)
    .put('/user/1')
    .set('authorization', token)
    .send({
      "name": "so",
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('successfully update user')
        done()
      }
    })
  })

  it('No Auth', (done) => {
    request(app)
    .put('/user/1')
    .send({
      "name": "di",
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Not found', (done) => {
    request(app)
    .get('/user/1000')
    .set('authorization', token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(404)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('user not found')
        done()
      }
    })
  })
})


describe('DELETE user', () => {

  it('Success', (done) => {
    request(app)
    .delete('/user/1')
    .set('authorization', token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('successfully delete user')
        done()
      }
    })
  })

  it('No auth', (done) => {
    request(app)
    .delete('/user/1')
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Not found', (done) => {
    request(app)
    .get('/user/1000')
    .set('authorization', token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(404)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('user not found')
        done()
      }
    })
  })
})
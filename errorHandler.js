module.exports = (error, req, res, next) => {
  if (error.status) {
    res.status(error.status).json({
      message: error.message,
    })
  } else {
    console.log(error)
    res.status(500).json({
      message: 'Internal Server Error'
    })
  }
}
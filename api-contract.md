# Todo API Contract

## API Contract (26 April 2022)

### POST /login

#### Success

- Request

```json
{
  "name": "dhimas",
  "password": "di"
}
```

- Response 200

```json
{
  "token": "${string}"
}
```

#### Wrong Password

- Request

```json
{
  "email": "dhimas",
  "password": "ad21" 
}
```

- Response (401)

```json
{
  "message": "Invalid email or password"
}
```

#### Wrong name

- Request

```json
{
  "email": "dhim",
  "password": "di" 
}
```

- Response (401)

```json
{
  "message": "Invalid email or password"
}
```

### GET /user

#### Success

- Response(200)

```json
[
  {
  "name": "dhimas",
  "passord": "di"
  }
]
```

#### internal server error

- Headers

```json
{}
```

- Response (500)

```json
{
  "message": "Internal Server Error"
}
```


### GET /todos/:id

#### Success (200)

- Headers

```json
{
  "id": "1"
}
```

- Response

```json
{
  "name": "dhimas",
  "password": "di",
  "id": "1"
}
```
#### Not found (404)

- Headers
```json
{
}
```
- Params
```json
{
  "id": 1000
}
```
- Response
```json
{
  "message": "User not found"
}
```

#### Internal server error (500)

- Headers
```json
{
}
```
- Response
```json
{
  "message": "Internal Server error"
}
```

### Update /todos/:id

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{
  "name": "dhimas",
  "password": "dhimas123"
}
```

- Response (200)

```json
{
  "message": "Successfully update user"
}
```

#### No auth
- Headers

```json
{}
```

- Body

```json
{
  "name": "dhimas",
  "password": "dhimas",
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid token
- Headers

```json
{
  "authorization": "Qweqweqwe"
}
```

- Body

```json
{
  "name": "dhimas",
  "passwrod": "dhimas"
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

### Delete Todo

#### Success

- Headers
```json
{
  "id": "2"
}
```

- Response (200)
```json
{
  "message": "Successfully delete todo"
}
```
#### Not found (404)

- Headers
```json
{
}
```
- Params
```json
{
  "id": 1000
}
```
- Response
```json
{
  "message": "User not found"
}
```
#### Internal server error (500)

- Headers
```json
{
}
```
- Response
```json
{
  "message": "Internal Server error"
}
```



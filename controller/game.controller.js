const { Usergame } = require('../models')
const jwt = require('jsonwebtoken');
const { OAuth2Client } = require('google-auth-library')
const axios = require('axios')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET)
const sendEmail = require('../services/sendEmail.service')

class GameController {
 static async login (req, res, next) {
    try {
      const user = await Usergame.findOne({
        where: {
          name: req.body.name
        }
      })
      if (!user) {
        throw {
          status: 401,
          message: 'invalid username or password'
        }
      } else {
        if (req.body.password==user.password) {
          // mengeluarkan token
          let token = jwt.sign({ id: user.id, name: user.name },process.env.JWT_SECRET);
          res.status(200).json({
            message: 'login success',
            token,
          })
          // console.log(token)
        } else {
          throw {
            status: 401,
            message: 'invalid username or password'
          }
        }
      }
     } catch (error) {
      next(error);
     }
    // masukkan username dan password
    // cek apakah username ada di database, kalau ada, lanjut proses selanjutnya, kalau misalnya username
    // tidak ada di database, maka error (tidak bisa dapat token)
    // username ada di database => mencocokan password, kalau cocok dapet token, kalau tidak, tidak bisa masuk (tidak dapat token)
   }



  static async list(req, res) {
    Usergame.findAll({
      attributes: ['id', 'name','video'],
    })
      .then((data) => {
        res.status(200).json(data)
      })
      .catch((error) => {
        res.status(500).json({error})
      })
  }

  static async getById(req, res, next) {
    try {
      console.log(req.user)
      const user = await Usergame.findOne({
         where: {
          id: req.user.id,
        },
      })
      if (!user) {
        throw {
          status: 404,
          message: 'user not found'
        }
      } else {
        res.status(200).json(user)
      }
    } catch (err) {
      next(err)
    }
  }

  static async create(req, res, next) {
    try {
        if (req.file) {
        req.body.video = `http://localhost:3000/${req.file.filename}`
      }
      await Usergame.create({
        name: req.body.name,
        password: req.body.password,
        email:req.body.email,
        video: req.body.video
      })
       const html = `
        <pre>
        <H1>Welcome To Game App ,${req.body.name}!</H1>
        <pre>
        `
        await sendEmail('dhimweray222@gmail.com', req.body.email, html, null, req.body.email)
        res.status(200).json({
          message: 'Succesfully send email'
        })
     } catch (error) {
      next(error)
    }
   
  }

  static async update(req, res,next) {
     try {
      const user = await Usergame.findOne({
         where: {
          id: req.user.id,
        },
      })

      if (!user) {
        throw {
          status: 404,
          message: 'user not found'
        }
      } else {
        await Usergame.update(req.body, {
          where: {
            id: req.params.id
          }
        })
        res.status(200).json({
          message: 'successfully update user'
        })
      }
    } catch (err) {
      next(err)
    }   
  }

  static async delete(req, res, next) {
    try {
      const user = await Usergame.findOne({
         where: {
          id: req.user.id,
        },
      })
      if (!user) {
        throw {
          status: 404,
          message: 'user not found'
        }
      } else {
      Usergame.destroy({
      where: {
        id: req.params.id
      }
      })
        res.status(200).json({
          message: 'successfully delete user'
        })
      }
    } catch (err) {
      next(err)
   }
  }


  static async google(req, res, next) {
    try {
      const token = await client.verifyIdToken({
        idToken: req.body.id_token,
        audience: process.env.GOOGLE_CLIENT_IDs
      })
      const payload = token.getPayload()

      const admin = await Usergame.findOne({
        where: {
          email: payload.email
        }
      })

      if (admin) {
        throw {
          status: 400,
          message: 'Invalid email'
        }
      }
      const user = await Usergame.findOne({
        where: {
          email: payload.email
        }
      })
      if (user) {
        const jwtToken = jwt.sign({
          id: user.id,
          email: user.email
        }, process.env.JWT_SECRET)
        res.status(200).json({
          token: jwtToken
        })
      } else {
        const registeredUser = await Usergame.create({
          email: payload.email
        })
        const jwtToken = jwt.sign({
          id: registeredUser.id,
          email: registeredUser.email
        }, process.env.JWT_SECRET)
        res.status(200).json({
          token: jwtToken
        })
      }
    } catch(err) {
      next(err)
    }
  }
}



module.exports = GameController
const express = require('express')
const router = express.Router()
const UsergameController = require('../controller/user.controller')

router.post('/send-forgot-pass-token', UsergameController.sendForgotPasswordToken)
router.post('/verify-forgot-pass-token', UsergameController.verifyForgotPasswordToken)
router.post('/change-pass', UsergameController.changePassword)

module.exports = router
const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const GameController = require('../controller/game.controller.js')
const { body, validationResult } = require('express-validator');

const multer = require('multer')
const storage = require('../services/multerStorage.service')
const upload = multer({
  storage,
  limits: {
    fileSize: 10000000
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype.match('video')) {
      cb(null, true)
    } else {
      cb(Error, false)
    }
  }
})

router.post('/login-google', GameController.google)

router.post('/', GameController.login)
router.get('/user',
(req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
    }
  }
  }, GameController.list)
  
  
router.get('/user/id',
  (req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
      
    }
  }
  },
  body('id').notEmpty(),
  GameController.getById)


router.post('/user',
  upload.single('video'),
  [
  body('name').notEmpty(),
  body('password').notEmpty(),
],
  (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: "Data cannot be empty" 
      }
    } else {
      next()
    }
  },GameController.create
)


router.put('/user/:id',
  (req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
    }
  }
},
  [
  body('password')
  .optional()
  .notEmpty()
  ],
  (req, res, next) => {
  const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: "Name or Password must be filled"
      }
    } else {
      next()
    }
  },
  GameController.update)


router.delete('/user/:id', (req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
    }
  }
}, GameController.delete)


  
module.exports = router
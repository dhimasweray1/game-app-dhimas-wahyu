const express = require('express')
const router = express.Router()
const userRoutes = require('./usergame.route')
const user = require('./user.route')


router.use('/', userRoutes)
router.use('/user', user)

module.exports = router